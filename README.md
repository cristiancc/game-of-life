# Game of Life

Ruby version of Game of Life.

## Instructions

Launch the Ruby interactive interpreter:

`irb -r ./game_of_life`

Initialize the game file passing a pattern:

`rbi > GameOfLife.new(:toad)`
