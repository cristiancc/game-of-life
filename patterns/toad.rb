#
# Toad pattern class
#
class Toad
  #
  # Init method
  #
  # @return [True]
  def initialize; end

  #
  # Pattern method
  #
  # @return [Array]
  def pattern
    [[2, 2], [2, 3], [2, 4], [3, 1], [3, 2], [3, 3]]
  end
end
