require './universe'
require './cell'
require './patterns/toad'

#
# Game of life class
#
class GameOfLife
  #
  # Init method
  #
  # @param pattern [Symbol] Pattern name
  #
  # @return [Boolean]
  def initialize(pattern)
    @pattern_name = pattern.to_s

    @pattern = find_pattern(@pattern_name)

    @universe_size = 6

    @universe = Universe.new(@pattern, @pattern_name, @universe_size)

    @universe.output(0)

    next_generation
  end

  #
  # Output next generation
  #
  # @return [Boolean]
  def next_generation
    cells = @universe.cells

    pattern = cells.select { |x, y| neighbours(x, y) }

    universe = Universe.new(pattern, @pattern_name, @universe_size)

    universe.output(1)
  end

  private

  #
  # Pattern lookup
  #
  # @param name [Symbol] Pattern name
  #
  # @return [Array]
  def find_pattern(name)
    string = name.capitalize()

    return exit! unless Object.const_defined?(string)

    Object.const_get(string).new.pattern
  end

  #
  # Find neighbours
  #
  # @param x [Integer] X coord for the living cell we want to find neigbours
  # @param y [Integer] Y coord for the living cell we want to find neigbours
  #
  # @return [Boolean]
  def neighbours(x, y)
    neighbours = [
      [0, x - 1].max .. [x + 1, @universe_size - 1].min,
      [0, y - 1].max .. [y + 1, @universe_size - 1].min
    ]

    destiny!(neighbours, x, y)
  end

  #
  # Set destiny of next generation cells
  #
  # @param neighbours [Array] Bidemensional array with ranges for x/y coords
  # @param _x [Integer] X coord for the living cell we want to find neigbours
  # @param _y [Integer] Y coord for the living cell we want to find neigbours
  #
  # @return [Boolean]
  def destiny!(neighbours, _x, _y)
    result = 0

    for x in neighbours[0]
      for y in neighbours[1]
        if x != _x || y != _y
          result += @universe[x, y].instance_variable_get(:@state)
        end
      end
    end

    result == 3 || (result == 2 && @universe[_x, _y].alive?)
  end
end
