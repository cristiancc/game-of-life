#
# Universe class
#
class Universe
  #
  # Init method
  #
  # @param pattern [Symbol] Pattern name
  # @param size [Integer] Universe size
  #
  # @return [Boolean]
  def initialize(pattern, pattern_name, size)
    @pattern_name = pattern_name

    @size = size

    @universe = build_universe

    fill_universe(pattern)
  end

  #
  # Output universe
  #
  # @param generation [Integer] Generation version number
  #
  # @return [Boolean]
  def output(generation)
    puts "!Name: #{@pattern_name.capitalize()} – Generation: #{generation}"

    puts print_universe
  end

  #
  # Yield cells or return enumerator if no block is provided
  #
  # @param &block [Block] Yielded block
  #
  # @return [Boolean]
  def cells(&block)
    return to_enum(__method__) unless block_given?

    @size.times { |x| @size.times {|y| yield(x, y) } }
  end

  #
  # Syntactic sugar method to access universe cells
  #
  # @param x [Integer] X coord we want to access
  # @param y [Integer] Y coord we want to access
  #
  # @return [Object]
  def [](x, y)
    @universe[x][y]
  end

  private

  #
  # Build an universe of dead cells
  #
  # @return [Array]
  def build_universe
    Array.new(@size) { Array.new(@size) { Cell.new(0) } }
  end

  #
  # Fill universe with living cells
  #
  # @return [Boolean]
  def fill_universe(pattern)
    pattern.each do |x, y|
      cell = @universe[x][y]
      cell.instance_variable_set(:@state, 1)
    end
  end

  #
  # Print universe
  #
  # @return [Array]
  def print_universe
    @universe.map { |row| row.map {|cell| cell.dead? ? '.' : 'O'}.join(' ') }
  end
end
