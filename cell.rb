#
# Cell class
#
class Cell
  #
  # Init method
  #
  # @param state [Intefer] `0` for dead cells and `1` for living cells
  #
  # @return [Boolean]
  def initialize(state)
    @state = state
  end

  #
  # Check if cell is alive
  #
  # @return [Boolean]
  def alive?
    !@state.zero?
  end

  #
  # Check if cell is dead
  #
  # @return [Boolean]
  def dead?
    @state.zero?
  end
end
